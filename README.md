![Packagist Version](https://img.shields.io/packagist/v/jdoubleu/wp-dev-collection)

# WordPress Development Collection
Collection of useful WordPress plugins to assist during development.

This package just bundles these plugins, so you can easily install them all together with:
```
composer require --dev jdoubleu/wp-dev-collection
```

The plugins are installed through [wpackagist](https://wpackagist.org/), a composer repository which mirrors the WordPress plugin directory.

## Contributions
Any suggestion are welcome!
